import { connect } from 'react-redux';
import store from '../js/store.js';
import _ from 'lodash';

export const Description = React.createClass({
  render: function() {
    return (
      <div id="description-container">
        {this.props.imageUrl && <img src={this.props.imageUrl} id="descript-img"/>}
        {this.props.descText && <span id="descript-desc">{this.props.descText}</span>}
      </div>
    )
  }
});

function mapStateToProps(state) {
  var selected = state.selection;
  var descType = selected.descType;
  var props = {};
  props.actorIndex = selected.actorIndex;
  props.skillIndex = selected.skillIndex;

  var baseRef = {};

  if (descType === "CHARACTER")
    baseRef = _.get(state, ['characterInfos', selected.actorIndex], undefined);
  if (descType === "PLAYER")
    baseRef = _.get(state, ['playerInfos', selected.actorIndex], undefined);
  if (descType === "SKILL")
    baseRef = _.get(state, ['characterInfos', selected.actorIndex, 'skills', selected.skillIndex], undefined);
  if (_.isUndefined(baseRef)) return {};

  props.descText = baseRef.descText;
  props.imageUrl = baseRef.imageUrl;
  props.name = baseRef.name;

  if (descType === "PLAYER") {
    props.rank = baseRef.rank;
    props.level = baseRef.level;
    props.ratio = baseRef.ratio;
  } else if (descType === "SKILL") {
    props.cost = baseRef.cost;
    props.classes = baseRef.classes;
    props.cd = baseRef.cd;
  }

  return props;
}

export const DescContainer = connect(mapStateToProps)(Description);
