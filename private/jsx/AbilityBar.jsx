import { connect } from 'react-redux';
import { AbilityContainer } from './Ability.jsx';

const AbilityBar = React.createClass({
  propTypes: {
    charIndex: React.PropTypes.number.isRequired
  },

  render: function() {
    var self = this;
    return (
      <div className="abilityBar">
        {new Array(4).fill(0).map(function(e, i) {
        return (
          <AbilityContainer charIndex={this.props.charIndex} skillIndex={i} key={i}/>
        )
      }, this)
      }
          </div>
    )
  }
});

export default AbilityBar;
