import { BarTimerContainer } from './BarTimer.jsx'

import store from '../js/store.js';
import { connect } from 'react-redux';
import _ from 'lodash';


export const EnergyManagement = React.createClass({
  render: function() {
    return (
      <div id="energy-management">
        <span>
        {
      this.props.isEnemyTurn
        ? "WAITING FOR OPPONENT"
        : "PRESS WHEN READY"
      }</span>
        {this.props.totalEnergy.map(function(value, i) {
        return (<span className={this.props.energyLegend[i]} key={i}>{this.props.totalEnergy[i]} </span>);
      }, this)
      }
        {this.props.totalEnergy.map(function(value, i) {
        return (<span className={this.props.energyLegend[i]} key={i}>{this.props.totalEnergy[i]} </span>);
      }, this)
      }

        <br/><br/>

        {this.props.freeEnergy.slice(0, 4).map(function(value, i) {
        return (<span className={this.props.energyLegend[i]} key={i}>{this.props.freeEnergy[i]} </span>);
      }, this)
      }
        <span className='totalEnergy' key='4'>{this.props.freeEnergy[4]}</span>
      	<BarTimerContainer/>
      </div>
    )
  }
});

function mapStateToProps(state, ownProps) {
  let freeEnergy = state.gameInfo.energy.getAvailable();
  let totalEnergy = state.gameInfo.energy.getTotal();

  return {
    isEnemyTurn: state.isEnemyTurn,
    freeEnergy,
    totalEnergy,
    energyLegend: ["red", "white", "blue", "yellow"]
  }
}

export const EnergyManagementContainer = connect(mapStateToProps)(EnergyManagement);
