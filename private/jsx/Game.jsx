import { PlayerInfoContainer } from './PlayerInfo.jsx';
import ActionBars from './ActionsBar.jsx';

import { EnergyManagementContainer } from './EnergyManagement.jsx';
import { DescContainer } from './Description.jsx';
import { TeamBlockContainer } from './TeamContainer.jsx';
import { Provider } from 'react-redux';
import store from '../js/store.js';

var socket = require('socket.io-client')();


socket.on('msgReceived', function(data) {
  console.log(data);
});

function sendMsg() {
  socket.emit('send message', {
    msg: "this is a message test"
  })
}



export const Game = React.createClass({
  propTypes: {
    allyCharacters: React.PropTypes.array.isRequired,
    enemyCharacters: React.PropTypes.array.isRequired
  },

  intialiseData: function(data) {
    store.dispatch({
      type: "SET_DESCRIPTION",
      imageUrl: data.description.imageUrl,
      description: data.description.text
    });

    store.dispatch({
      type: "SET_PLAYERINFOS",
      playerInfos: data.playerInfos
    });

    store.dispatch({
      type: "SET_ALL_CHARINFO",
      charInfos: data.characterInfos
    });

    store.dispatch({
      type: "SET_GAME_INFO",
      totalEnergy: [3, 3, 3, 3],
      exisitingSkills: []
    });

    store.dispatch({
      type: "START_TIMER",
      turnTime: data.timer.turnTime
    });

    store.dispatch({
      type: "QUEUE_SKILL",
      charIndex: 1,
      skillIndex: 0,
      targetIndexes: [1],
      cost: store.getState().characterInfos[0].skills[1].cost
    });

    store.dispatch({
      type: "QUEUE_SKILL",
      charIndex: 2,
      skillIndex: 1,
      targetIndexes: [1],
      cost: store.getState().characterInfos[1].skills[0].cost
    });

    store.dispatch({
      type: "DEQUEUE_SKILL",
      charIndex: 2,
    });
  },

  componentDidMount: function() {
    let self = this;
    this._socket = require('socket.io-client')();

    this._socket.on('receiveGame', function(data) {
      console.log(data);
      self.intialiseData(data);
    });

    this._socket.emit('requestGame');
  },

  render: function() {
    return (
      <Provider store={store}>
        <div id='game-container'>
          <div id="game-header">
          <PlayerInfoContainer
      isPlayerEnemy={true}
      ></PlayerInfoContainer>
          <EnergyManagementContainer></EnergyManagementContainer>
          <PlayerInfoContainer
      isPlayerEnemy={false}
      ></PlayerInfoContainer>
        </div>
        <div id="character-rows">
          <TeamBlockContainer
      isEnemyTeam={false}
      ></TeamBlockContainer>
          <TeamBlockContainer
      isEnemyTeam={true}
      ></TeamBlockContainer>
        </div>

        <div id="footer-bar">
          <ActionBars/>
          <DescContainer/>
        </div>
      </div>
      </Provider>

    )
  }
});

ReactDOM.render(
  React.createElement(Game,
    {
      allyCharacters: [
        {
          portrait: './res/ichigo.jpg',
          description: "He is strong",
          name: "itsygo",
        },
        {
          portrait: './res/ichigo.jpg',
          description: "He is strong",
          name: "itsygo"
        },
        {
          portrait: './res/ichigo.jpg',
          description: "He is strong",
          name: "itsygo"
        }
      ],
      enemyCharacters: [
        {
          portrait: './res/ichigo.jpg',
          description: "He is strong",
          name: "itsygo"
        },
        {
          portrait: './res/ichigo.jpg',
          description: "He is strong",
          name: "itsygo"
        },
        {
          portrait: './res/ichigo.jpg',
          description: "He is strong",
          name: "itsygo"
        }
      ]
    }
  ),
  document.getElementById('content')
);
