import { CharContainer } from './Character.jsx';
import store from '../js/store.js';
import { connect } from 'react-redux';
import _ from 'lodash';


export const TeamBlock = React.createClass({
  propTypes: {
    isEnemyTeam: React.PropTypes.bool.isRequired,
  },

  render: function() {
    var self = this;
    var indxAdjust = this.props.isEnemyTeam * 3;
    return (
      <div id={this.props.isEnemyTeam ? "character-column-enemy" : "character-column-ally"}>
 			{new Array(3).fill(0).map(function(e, i) {
        return (
          <CharContainer
          key={i + indxAdjust}
          charIndex= {indxAdjust + i}
          ></CharContainer>
        )
      })}
      </div>
    )
  }

});


function mapStateToProps(state, ownProps) {
  var startIndex = ownProps.isEnemyTeam * 3;
  return {
    characters: state.characterInfos.slice[startIndex, startIndex + 3],
    isEnemyTeam: ownProps.isEnemyTeam
  }
}

export const TeamBlockContainer = connect(mapStateToProps)(TeamBlock);
