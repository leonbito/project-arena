import store from '../js/store.js';
import { connect } from 'react-redux';
import _ from 'lodash';

export const PlayerInfo = React.createClass({
  propTypes: {
    isPlayerEnemy: React.PropTypes.bool
  },

  handlePortraitOnClick: function() {
    store.dispatch({
      type: "SET_DESCRIPTION",
      actorIndex: (this.props.isPlayerEnemy) + 0,
      descType: "PLAYER"
    });
  },

  render: function() {
    return (
      <div id={this.props.isPlayerEnemy ? "player1-info" : "player2-info"}>
      	<img className="char-portrait" src={this.props.imageUrl} onClick={this.handlePortraitOnClick}></img>
      	<span>{this.props.playerName}</span>
      </div>
    )
  }

});

function mapStateToProps(state, cProps) {
  var pIndex = cProps.isPlayerEnemy ? 1 : 0;
  return {
    imageUrl: _.get(state, 'playerInfos[' + pIndex + '].imageUrl'),
    playerName: _.get(state, 'playerInfos[' + pIndex + '].name')
  }
}


export const PlayerInfoContainer = connect(mapStateToProps)(PlayerInfo);
