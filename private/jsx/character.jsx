
import AbilityBar from './AbilityBar.jsx';
import store from '../js/store.js';
import { connect } from 'react-redux';
import _ from 'lodash';

export const Character = React.createClass({
  propTypes: {
    charIndex: React.PropTypes.number.isRequired
  },

  handlePortraitOnClick: function() {
    store.dispatch({
      type: "SET_DESCRIPTION",
      actorIndex: this.props.charIndex,
      descType: "CHARACTER"
    });
  },

  render: function() {
    return (
      <div className='charContainer'>
        <div className='charLeft'>
          <img className="char-portrait" src={this.props.imageUrl} onClick={this.handlePortraitOnClick}></img>
          <div className="hp-bar">
            <span>{this.props.hp}/100</span>
          </div>
        </div>
        {this.props.charIndex < 3 &&
      <div className="charRight">
            <AbilityBar className="charRight" charIndex={this.props.charIndex}/>
          </div>
      }

      </div>
    )
  }

});


function mapStateToProps(state, ownProps) {
  var characters = state.characterInfos;
  var cIndex = ownProps.charIndex;

  return {
    hp: 100,
    charName: _.get(characters, [cIndex, 'charName'], ''),
    charIndex: cIndex,
    imageUrl: _.get(characters, [cIndex, 'imageUrl'], ''),
  }

}

export const CharContainer = connect(mapStateToProps)(Character);
