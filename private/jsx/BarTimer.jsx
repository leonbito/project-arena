import { connect } from 'react-redux';
import _ from 'lodash';

export const BarTimer = React.createClass({
  getInitialState: function() {
    return {
      timePercentage: 100
    };
  },

  componentDidMount: function() {
    this._timer = setInterval(this.tick, 1000);
  },

  componentWillUnmount: function() {
    if (this._timer) {
      clearInterval(this._timer);
      this._timer = null;
    }
  },

  tick: function() {
    if (this.props.timerState === 'RUNNING') {
      let percentElapsed = 100 * (Date.now() - this.props.startTime) / this.props.turnTime;
      this.setState({
        timePercentage: Math.min(Math.max(100 - Math.round(percentElapsed), 0), 100)
      });
    }
  },

  render: function() {
    return (
      <div className='barTimerContainer'>
        <div className='barTimer' style={{
        width: this.state.timePercentage + '%'
      }}>
        </div>
      </div>
    )
  }

});

function mapStateToProps(state) {
  return {
    timerState: _.get(state, 'timer.timerState'),
    turnTime: _.get(state, 'timer.turnTime'),
    startTime: _.get(state, 'timer.startTime')
  };
}


export const BarTimerContainer = connect(mapStateToProps)(BarTimer);
