import { connect } from 'react-redux';


export const Ability = React.createClass({
  propTypes: {
    skillIndex: React.PropTypes.number.isRequired,
    charIndex: React.PropTypes.number.isRequired
  },

  render: function() {
    return (
      <div className="ability">
        <img src={this.props.imageUrl}/>
      </div>
    )
  }
});

function mapStateToProps(state, ownProps) {
  var skill = _.get(state, ['characterInfos', ownProps.characterIndex, 'skills', ownProps.skillIndex])

  return {
    skillIndex: ownProps.skillIndex,
    charIndex: ownProps.charIndex,
    imageUrl: _.get(skill, ['imageUrl'], "res/ichigo.jpg"),
    cd: 0,
    usable: true
  }
}


export const AbilityContainer = connect(mapStateToProps)(Ability);
