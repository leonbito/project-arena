
var EffectFactory =  require('./effects/EffectFactory.js')
var Energy = require('./energy.js');
var _ = require('lodash');

function Game(chars){
  var g = {};
  g.characters = chars;
  g.players = [];
  g.playerToMove = null;
  g.moveTimer = null;


  g.initGame = function(){
    for(let i = 0; i < 2; i++){
      let energy = Energy();
      energy.gain(i*2+1);
      g.players.push({
        energy: energy
      });
    }
  }

  function getCharacter(charIndex) {
    return this.characters[charIndex];
  }

  function getModifiers(effect, targets){
    return _.reduce(targets, function (modifiers, target){
      return  modifiers.concat(_.filter(character.modifiers, function (e){
        return _.instersection(effect.types, e.types).length;
      }));
    }, []);
  }

  function getTriggers(ability, target){
    return _.filter(character.triggers, function (e){
      return _.instersection(ability.effect, e.types).length;
    });
  }

  function processTriggers(ability) {
    _.each(ability.targets, function(target){
      let triggers = getTriggers(ability, target);
      
      for(var trigger in triggers) {
        let result = trigger.activate(ability);
        if(result.code === 0){
          ability = result.ability;
        } 
        else if (result.code === 1){
          return result.ability;
        }
        else if (result.code === 2){
          return processTriggers(result.ability);
        }
      }
      return ability;
    });
  }

  g.charactersAliveCount = function(playerIndex){
    _.reduce(getCharacters(playerIndex), function (total, char){
      return (total + ((char.hp === 0) ? 1: 0));
    }, 0)
  }

  g.getCharacters = function(team){
    return (characters.slice(team*3,3));
  }

  g.processTurn = function(turn){
    _.each(turn.abilities, function (abil){
      useAbility(abil);
    });
  }
  
  g.useAbility = function(ability){
    console.log(ability.effects);
    //trigger ability on self and targets
    ability = processTriggers(ability);
    //modify effect and use the resultant effect
    _.each(ability.effects, function (effect){
      let modifiers = getModifiers(effect, [ability.initiator, effect.target]);
      let finalEffect = _.reduce(modifiers, function(accumEffect, eMod){
        return eMod.modify(accumEffect);
      }, effect);
      finalEffect.affect(game);
    })
  }
  return g;
}

module.exports = Game;