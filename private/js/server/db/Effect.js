var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var effectSchema = Schema({
  name: { type : String , unique : true, required : true },
  value: { type : String , required : true },
  target: {type: Number, required: true},
  duration: {type: Number, required: true},
  tooltip: {type: String, required: false}
});

module.exports = mongoose.model('Effect', effectSchema);
