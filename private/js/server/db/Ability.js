var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var abilitySchema = Schema({
  name: { type : String , unique : true, required : true },
  description: { type : String , required : true },
  cost: {type: Array, required: true},
  effects: {type: Array, required: true},
  tooltip: {type: String, required: false}
});

module.exports = mongoose.model('Ability', abilitySchema);