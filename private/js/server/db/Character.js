var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = Schema({
  name: { type : String , unique : true, required : true },
  description: { type : String , required : true },
  abilities: { type : Array, required: true },
  tags: {type: String }
});

module.exports = mongoose.model('Character', userSchema);