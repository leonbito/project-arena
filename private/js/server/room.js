var uuid = require('node-uuid');
var _ = require('lodash');

function Room(players){
  var r = {};

  r.roomID = uuid.v1();
  r.users = {};

  var characters = [];
  _.map(players, function(p){
    users[p.username] = {disconnectedTimeout: null};
    characters.concat(p.characters);
  });

  r.game = Game(characters);
  r.game.initGame();
  return r;
}
