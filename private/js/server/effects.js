
var Effect = function (data){
  return {
    name: data.name,
    value: data.value,
    target: data.target,
    duration: data.duration
  }
}

Module.exports = Effect;