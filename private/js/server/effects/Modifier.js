var Modifiers = function(data){
	var m = Effect(data);
	delete m.target;
	m.modify = function(effect){
		effect.value += m.value;
	}
}

module.exports = Modifiers;