var Effect = require('./Effect.js');

var RemoveEnergy = function (data){
  var e = Effect(data);
  e.affect = function(game){
      let team =(_.inRange(data.target,3) ? 0: 1);
      players[team].energy.RemoveEnergy(team,null)
  }
  return e;

}


module.exports = RemoveEnergy;