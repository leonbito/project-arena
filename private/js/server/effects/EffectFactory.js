var Effect = require('./Effect.js');
var Damage = require('./Damage.js');
var DamageReduction = require('./DamageReduction.js');
var Modifier = require('./Modifier.js');
var RemoveEnergy = require('./RemoveEnergy.js');

var EffectFactory = function (data){
	return (this[data.name])(data);	
}


module.exports = EffectFactory;