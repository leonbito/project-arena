
var Ability = function(abilityInfo, target, initiator){
  var a = {};
  a.name = abilityInfo.name;
  a.description = abilityInfo.description;
  a.imageUrl = abilityInfo.imageUrl;
  a.initiator = initiator;
  a.targets = [];
  a.effects = [];
  a.getTargets = function(target){
    switch (target) {
      case "self":
        return [a.initiator];
      case "target":
        return [target];
      case "all enemies":
        return (_.inRange(a.initiator,3) ? [3,4,5]: [0,1,2]);
      case "all allies":
        return (_.inRange(a.initiator,3) ? [0,1,2]: [3,4,5]);
      case "target team":
        return Math.floor(target / 3) ? [0,1,2] : [3,4,5];
    }
  }
  
  _.each(abilityInfo.effects, function (effectData){
    _.each(a.getTargets(effect.target), function(target){
      effectData.target = target;
      a.effects.push(EffectFactory(effectData));
      a.targets.push(target);
    });
  });
  
  a.traits = abilityInfo.traits;
  a.cooldown = abilityInfo.cooldown;
  a.tooltips = function(effects){
    _.reduce(effects, function(total, e){
      return total + e + "<br>";
    },"");
  };
  
  return a;
}

module.exports = Ability;