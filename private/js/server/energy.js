var _ = require('lodash');

var EnergySystem = function(){
  var es = {};
  es.energy = [0,0,0,0];
  es.cap = 5;

  es.gain = function(amount, type) {
    var uncapped = _.reduce(es.energy, function(total,e,i){
      return (e < es.cap) ? total.concat(i): total;
    }, []);

    _.times(amount, function(){
      if(uncapped.length === 0) return;
      let energyType = (_.includes(uncapped, type)) ? type : uncapped[(_.random(0,uncapped.length -1))];
      es.energy[energyType]++;
      if (es.energy[energyType] === es.cap){
        _.remove(uncapped, (n) => { return (n === energyType);})
      }
    });
  };

  es.remove = function(amount, type){
    var notEmpty = _.reduce(es.energy, function(total,e,i){
      return (e > 0) ? total.concat(i): total;
    }, []);

    _.times(amount, function(){
      if(notEmpty.length === 0) return;
      let energyType = (_.includes(notEmpty, type)) ? type : notEmpty[(_.random(0,notEmpty.length -1))];
      es.energy[energyType]--;
      if (es.energy[energyType] === 0){
        _.remove(notEmpty, (n) => { return (n === energyType);})
      }
    });
  }

  es.drain = function(amount,type){
    
  }

  es.transmute = function(costs, type){

  }

  es.use = function(costs){
    let newEnergy = _.map(_.zipWith(es.energy, costs), function(e){
      return e[0] - e[1];
    });

    for (var i = 0; i < newEnergy.length;i++){
      if (newEnergy[i] < 0) return false;
    }

    es.energy = newEnergy;
    return true;
  }


  es.totalEnergyCount = function(){
    return _.reduce(es.energy, function(total, k){
      return (total + k);
    },0);
  };

  return es;
};

module.exports = EnergySystem;