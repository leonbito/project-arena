var _ = require('lodash');

function Character(characterData){
  var c = {};
  c.hp = 100;
  c.triggers = [];
  c.modifiers = [];
  c.effects = [];

  c.name = characterData.name;
  c.imageUrl = characterData.imageUrl;
  c.description = characterData.description;
  
  c.abilityData = _.map(characterData.abilityIDs, function(abilityID){
    return {id: abilityID, cd: 0};
  });
  

  return c;
}

module.exports = Character;