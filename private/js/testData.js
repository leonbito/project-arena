var char1 = {
  descText: "i'm gonna be hokage, believe it",
  imageUrl:"./res/ichigo.jpg",
  name:"kurosaki ichigo",
  skills:[
    {cost: [2,1,0,0,0],  classes:["physical"], cd:5, description: "skill #1"},
    {cost: [1,1,0,0,1],  classes:["physical"], cd:2, description: "skill #2"},
    {cost: [0,2,0,0,1],  classes:["physical"], cd:6, description: "skill #3"},
    {cost: [0,2,0,0,1],  classes:["physical"], cd:3, description: "skill #4"},
  ]
}

var char2 = {
  descText: "i'm 2nd character",
  imageUrl:"./res/ishida.jpg",
  name:"Baron von Yolo",
  skills:[
    {cost: [0,1,0,0,0],  classes:["physical"], cd:5, description: "skill #11"},
    {cost: [0,2,0,0,1],  classes:["physical"], cd:2, description: "skill #22"},
    {cost: [0,2,0,0,0],  classes:["physical"], cd:6, description: "skill #33"},
    {cost: [0,2,0,0,0],  classes:["physical"], cd:3, description: "skill #44"},
  ]
};


var playl = {
  name: 'Absolute Madman',
  imageUrl: './res/ichigo.jpg',
  descText: 'he actually did it',
  rank: "noob",
  level: 137,
  ratio: '1-37'
};

var play2 = {
  name: 'Exodia the Forbidden One',
  descText:'My grandfathers deck has no pathetic cards',
  imageUrl: './res/ishida.jpg',
  rank: "noob",
  level: 137,
  ratio: '1-37'
};

var timer = {
  turnTime: 60000, 
};

module.exports = function fetchData(){
  return {
    description: {
      text: 'placeholder',
      imageUrl: './res/ichigo.jpg'
    },

    playerInfos: [ playl, play2 ],
    characterInfos: [char1, char2, char1, char2, char1, char2],
    totalRei: [3,3,3,3],
    existingSkills: [],
    timer: timer
  }
}
