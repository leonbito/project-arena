var _ = require('lodash');
import store from './store.js';

function Energy (total) {
  let totalEnergy = total;

  let availableEnergy = function(){
    if (!store) return [0,0,0,0,0];

    let maxEnergy = _.sum(totalEnergy);
    let abilityCosts = _.map(store.getState().gameInfo.skillsUsed, (o)=> {return o.cost});
    if (_.isEmpty(abilityCosts)) return totalEnergy.concat(maxEnergy);

    let totalCosts = _.zipWith(...abilityCosts, function(a=0, b=0, c=0){
      return a+b+c;
    });
    let limit = maxEnergy - (_.sum(totalCosts));

    //limit energy available of specific to match total rei
    //i.e. have 5 rei total (4 reds). spend 3 randoms. can only use 2 reds not 4.
    let energyReserved = _.zipWith(totalEnergy, totalCosts.slice(0,4), (a,b)=>{
      return ((a-b) < limit) ? (a-b) : limit;
    });

    let reservedTotal = energyReserved.concat(maxEnergy - (_.sum(totalCosts)));
    return reservedTotal;
  };

  return {
    getTotal: (() => totalEnergy),
    setTotal: ((total) => {totalEnergy = total}),
    getAvailable: availableEnergy,
  }
}

export default Energy;
