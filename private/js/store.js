import { createStore, combineReducers } from 'redux';
import _ from 'lodash';
import Energy from './energyCalc.js'

const characterInfos = (state = [], action) =>{
  var newState = [...state];
  if (action.type === "SET_ALL_CHARINFO"){
    newState = action.charInfos;
  }
  return newState;
};


const gameInfo = (state = {
  energy: Energy([0,0,0,0]),
  skillsUsed: [],
}, action) => {
  var nState = _.cloneDeep(state);
  switch (action.type) {
    case "SET_GAME_INFO":
      nState.energy.setTotal(action.totalEnergy);
      break;
    case "QUEUE_SKILL":
       nState.skillsUsed.push ({
        charIndex: action.charIndex,
        skillIndex: action.skillIndex,
        targetIndexes: action.targetIndexes,
        cost: action.cost
      });
      break;
    case "DEQUEUE_SKILL":
      var skillIndex = _.findIndex(nState.skillsUsed, function(o){return (o.charIndex === action.charIndex)});
      if (skillIndex !== -1) nState.skillsUsed.splice(skillIndex, 1);
      break;
  }
  return nState;
}


const selection = (state = {}, action) => {
  var newState = {...state};
  if (action.type === "SET_DESCRIPTION"){
    newState.descType = action.descType;
    newState.actorIndex = action.actorIndex;
    newState.skillIndex = _.get(action, 'skillIndex', null);
  }
  return newState;
};

const playerInfos = (state= {}, action) => {
    var newState = {...state};
    if (action.type === "SET_PLAYERINFOS" || action.type === "SET_ALL"){
        newState = {...action.playerInfos};
    }
    return newState;
};

const initialTimer = {
	timerState: "STOPPED",
	turnTime: 0,
	startTime: 0
};

const timer = (state= initialTimer, action) => {
    var newState = {...state};
    if (action.type === "START_TIMER"){
        newState.timerState = "RUNNING";
		newState.turnTime = action.turnTime;
      	newState.startTime = Date.now();
    } else if (action.type === "STOP_TIMER"){
    	newState.timerState = "STOPPED";
    }
    return newState;
};

const main = combineReducers({
  playerInfos,
  characterInfos,
  selection,
  gameInfo,
  timer
});

let store = createStore(main);

export default store;
