var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;


var _ = require('lodash');
var Game = require('../private/js/server/game.js');
var Character = require('../private/js/server/character.js');
var Energy = require('../private/js/server/energy.js')


var effect1 = {
  "name": "Damage",
  "value": 15,
  "target": "target",
  "duration": 1,
  "traits":{"harmful": "physical"}
}


var effect2 = {
  "name": "DamageReduction",
  "value": 10,
  "target": "self",
  "duration": 1,
  "traits":{"helpful": "strategic"}
}


var effect3 = {
  "name": "RemoveEnergy",
  "value": 1,
  "target": "target",
  "duration": 1,
  "traits":{"harmful": "physical"}
}

var abilOne = {
  "name": "attack",
  "imageUrl": "http://vignette1.wikia.nocookie.net/hunterxhunter/images/3/30/Gon_Portrait.png",
  "description": "hunter gon freecs",
  "cooldown": 0,
  "effects": [effect1]
}

var testCharData = {
  "imageUrl": "http://vignette1.wikia.nocookie.net/hunterxhunter/images/3/30/Gon_Portrait.png",
  "name": "gon",
  "description": "he's like a samurai",
  "abilityIDs": [0,1,2,3]
}


var leorioData = {
  "imageUrl": "http://vignette3.wikia.nocookie.net/hunterxhunter/images/1/19/Leorio_Portrait_2.png",
  "name": "leorio",
  "description": "leorio paladinkight",
  "abilityIDs": [0,1,2,3]
}



//check that energy initiliases correctly
//all players at 100hp
//no modifiers, effects, triggers on them
//3 characters per team, *no duplicates*

//----------------------------------------------------------------------------------------------------------------------- *CHARACTER*


describe ('Character initialization', function(){
  var char;

  before(function(){
    char = Character(testCharData);
  })

  it ('should set all base attributes correctly', function(){
    expect(char.hp).to.equal(100);
    expect(char.triggers).to.be.empty;
    expect(char.modifiers).to.be.empty;
    expect(char.effects).to.be.empty;
  })

  it('should have a name', function() {
    expect(char.name).to.be.ok;
  });

  it('should have a description', function() {
    expect(char.description).to.be.ok;
  });

  it('should have an imageurl', function() {
    expect(char.imageUrl).to.be.ok;
  });

})

//----------------------------------------------------------------------------------------------------------------------- *ENERGY*

describe ('Energy System', function(){
  var es;

  beforeEach(function(){
    es = Energy();
  })

  it ('should start with no reiatsu', function(){
    expect(es.totalEnergyCount()).to.equal(0);
  })

  it('gain 3 random energies', function() {
    es.gain(3);
    expect(es.totalEnergyCount()).to.equal(3);
  });

  it ('should gain 4 specific', function(){
    es.gain(4,2);
    expect(es.energy[2]).to.equal(4);
    expect(es.totalEnergyCount()).to.equal(4);
  })

  it ('should overflow into another resource', function(){
    es.gain(es.cap+2, 0);
    expect(es.energy[0]).to.equal(es.cap);
    expect(es.totalEnergyCount()).to.equal(es.cap+2);
  })

  it ('should not use skill due to insufficient costs', function(){
    var costsMet =  es.use([1,1,1,1]);
    expect(costsMet).to.equal(false);
  })

  it ('should use skill as costs are met', function(){
    es.gain(2,0);
    es.gain(3,1);
    var costsMet = es.use([1,3,0,0]);
    expect(costsMet).to.equal(true);
    expect(es.energy[0]).to.equal(1);
    expect(es.energy[1]).to.equal(0);
  })
})



//----------------------------------------------------------------------------------------------------------------------- *GAME*


describe('Game initilisation', function() {
  var game;
  var p1;
  var p2;
  var characters = [];

  before(function(){
    game = Game(characters)
    game.initGame();
    p1 = game.players[0];
    p2 = game.players[1];
    _.times(6, function(){
    characters.push(Character(testCharData));
    });
  })

  it('should initialise with correct energy amounts', function() {
    expect(p1.energy.totalEnergyCount()).to.equal(1);
    expect(p2.energy.totalEnergyCount()).to.equal(3);
  });

});


