var express = require('express');
var router = express.Router();
var User = require('./private/js/server/db/User.js');


router.use(express.static(__dirname + '/public'));

//checks if a player is in a game
function inGame(req,res,next){
  next();
}

//i.e stop people from going to register/login when logged in already
function notAuthenticated(req,res,next){
  if (req.session && req.session.user){
    res.redirect('/');
  } else {
    next();
  }
}

function authenticated(req, res,next) {
  if (req.session && req.session.user){
    next();
  } else {
    res.redirect('/login');
  }
};

router.get('/', function(req, res) {
  res.sendFile(__dirname + '/public/testSocket.html');
});

router.get('/play',authenticated, inGame, function(req,res){
  res.sendFile(__dirname + '/public/ingame.html');
});


//!~  LOGIN / LOGOUT / REGISTER

router.get('/login', notAuthenticated, function(req,res){
  res.render('login');
});

router.get('/register', notAuthenticated, function(req,res){
  res.render('register');
})

router.post('/register', function (req,res){
  var username = req.body.username;
  var email = req.body.email;
  var password = req.body.password;
  var password_confirm = req.body.password_confirm;
  //validation
  req.checkBody('username','username is required').notEmpty().len(4,20);
  req.checkBody('email', 'email is required').notEmpty().len(1,80);
  req.checkBody('password', `password is incorrect format`).notEmpty().len(6,30);
  req.checkBody('password_confirm', `password doesn't match`).equals(req.body.password);
  req.checkBody('email', 'email is email').isEmail();

  var errors = req.validationErrors();
  if (errors) {
    return res.render('register', {errors:errors});
  } else {
    //create the user
    var newUser = new User();
    newUser.username = username;
    newUser.email = email;
    newUser.password = password;
    newUser.save(function(err,savedUser){
      if (err){
        return res.render('register', {errors: [{msg:'username taken'}]});
      } else {
        return res.redirect('/');
      }
    });
  }
});


router.post('/login', function(req,res){
  var username = req.body.username;
  var password = req.body.password;

  if (req.session && req.session.user) {
    //already logged in
    res.status(200);
    res.redirect('/');
  }

  User.findOne({username: username}, function(err,user){
    if (err){
      return res.render('login', {errors: [{msg:'Something went wrong, try again!'}]});
    }
    if (!user){
      return res.render('login', {errors: [{msg:'User does not exist'}]});
    }

    user.comparePassword(password, function(err, isMatch) {
      if ( isMatch && isMatch == true) {
        req.session.user = JSON.stringify(user);
        return res.redirect('/');
      } else {
        return res.render('login', {errors: [{msg:'Password is incorrect'}]});
      }
    });
  });
});

router.post('/findMatch', function(req,res){

});

router.get('/logout', function(req,res){
  req.session.destroy();
  res.redirect('/');
});

module.exports = router;
