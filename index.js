var _ = require('lodash');
var express = require('express');
var expressValidator = require('express-validator');
var session = require('express-session');
var uuid = require('node-uuid');
var routes = require('./routes');
var bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');
var fetchData = require('./private/js/testData.js');
const DB_URI = 'mongodb://localhost:27017/arenadb';
var mongoose = require('mongoose');
var MongoDBStore = require('connect-mongodb-session')(session);

var app = express();
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
var server = require('http').Server(app);
var io = require('socket.io')(server);

mongoose.connect(DB_URI);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Database connected:")
});
var store = new MongoDBStore({
  uri: DB_URI,
  collection: 'sessions'
}, function(error){});



app.set('port', (process.env.PORT || 5000));
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(expressValidator());


const sessionMiddleware = session({
  genid: function(req) {
    return uuid.v1();
  },
  store: store,
  resave: false,
  saveUninitialized: true,
  secret: 'uwotm8',
  cookie: {
    path : '/',
    httpOnly: false,
    maxAge: 3*24*60*60*1000
  }
});

app.use(sessionMiddleware);

app.use('/', routes);

server.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
});

//----------------------------------------------------------------------------------------------------------------------

var rooms = [];
var waitingPlayer = null;
var playingPlayers = {};


io.use(function(socket, next) {
  sessionMiddleware(socket.request, socket.request.res, next);
});

io.use(function(socket, next){
  if (socket.request.session && socket.request.session.user){
    var userDetails = JSON.parse(socket.request.session.user);
    next();
  } else {
    next(new Error('Authentication error'));
  }
});

io.on('connection', function (socket) {

  var username = socket.request.session.username;
  var roomID = playingPlayers[username];

  if (roomID){
    let timer = rooms[roomID].username.disconnectTimeout;
    clearTimeout(timer);
    timer = null;
  };

  socket.on('findMatch', function(){
    if (waitingPlayer){
      var room = Room();
      rooms.push(room);
      playingPlayers[username] = room.id;
      playingPlayers[waitingPlayer.username] = room.id;
      waitingPlayer = null;
      socket.emit('matchFound');
    } else {
      waitingPlayer = {
        username: username,
        socketID: socket.id
      };
    }
  });

  socket.on('disconnect', function () {
    if (playingPlayers[username]){
      var roomID = playingPlayers[username].roomID;
      rooms[roomID].username.disconnectTimeout = setTimeout( () => {rooms[roomID].surrender(username)}, 1000*60);
    }

    if (waitingPlayer.username === username) waitingPlayer = null;
  });

  socket.on('send message', function (data) {
    io.to('game1').emit('msgReceived', {
      "message": data.msg
    })
  });
});

