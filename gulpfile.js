var gulp = require('gulp');
var gutil = require('gulp-util');
var browserify = require('browserify');
var watch = require('gulp-watch');
var transform = require('vinyl-transform');
var less = require('gulp-less');
var rename = require('gulp-rename');
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");

var gamePath = "./private/jsx/Game.jsx";
var registerPath = './private/js/register.js';
var loginPath = './private/js/login.js';

var esformatter = require('gulp-esformatter');
var minify = require('gulp-minify');

gulp.task('watch', function () {
   gulp.watch('private/less/*.less', ['lesser']);
});

gulp.task('lesser', function(){
   return gulp.src('private/less/*.less')
       .pipe(less())
       .pipe(rename({
          extname: ".css"
       }))
       .pipe(gulp.dest('./public/css'))
});


gulp.task('prettify', function(){
  return gulp.src('private/jsx/*.jsx')
        .pipe(esformatter({indent: {value: '  '}}))
        .pipe(gulp.dest('private/jsx/'));
});

gulp.task('browserify', function() {
    browserify({entries: [gamePath], debug: true})
        .transform("babelify", {presets: ["react", "es2015"], plugins: ["transform-object-rest-spread"]})
        .bundle()
        .on('error', function(e){
            gutil.log(e);
        })
        .pipe(source('bundle.jsx'))
        .pipe(buffer())
        .pipe(rename("script.js"))
        .pipe(gulp.dest('./public/js/'));
});

gulp.task('start', function(){
    gulp.start('browserify','lesser');
});


